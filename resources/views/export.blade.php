@if($request->type=='excel')
	@php
    	header("Content-type: application/vnd.ms-excel");
    	header("Content-Disposition: attachment;Filename=User.xls");
    @endphp
@endif
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <style>
		table {
		    border-collapse: collapse;
		    width: 100%;
		}

		table, td, th {
		    border: 1px solid black;
		    padding: 4px;
		}
	</style>
</head>
<body>
	<table>
		<thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Email</th>
                <th>User Role</th>
            </tr>
        </thead>
        <tbody>
            @php 
            	$i=0; 
            @endphp
            @foreach($users as $user)
                <tr>
                    <td>{{ ++$i }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->UserRole->user_role }}</td>
                </tr>
            @endforeach
        </tbody>
	</table>
</body>
</html>