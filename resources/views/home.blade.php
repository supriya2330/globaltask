@extends('layout')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Users
                    <span class="float-right">
                        <a target="_blank" href="{{ url('/export?type=pdf') }}" class="btn btn-sm btn-success">PDF</a>
                        <a target="_blank" href="{{ url('/export?type=excel') }}" class="btn btn-sm btn-primary">Excel</a>
                    </span>
                </div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>User Role</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $i=$users->perPage() * ($users->currentPage()-1); @endphp
                            @foreach($users as $user)
                                <tr>
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->UserRole->user_role }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @if($users->total() > $users->perPage())
                    <div class="card-footer"> 
                        <div class="row float-right">
                            <div class="col-md-12">
                                {{ $users->render() }}
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
