<?php

Route::get('/','HomeController@home')->name('home');

Route::get('/register','HomeController@register')->name('register');
Route::post('/register','HomeController@signup')->name('signup');
Route::get('/login','HomeController@login')->name('login');
Route::post('/login','HomeController@signin')->name('signin');

Route::get('/home','HomeController@home')->name('home');
Route::get('/export','HomeController@export')->name('export');
Route::get('/profile','HomeController@profile')->name('profile');
Route::post('/profile','HomeController@update_profile')->name('update_profile');
Route::get('/logout','HomeController@logout')->name('logout');