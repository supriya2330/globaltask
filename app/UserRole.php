<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class UserRole extends Model
{
    protected $fillable = [
        'user_role',
    ];

    protected $primaryKey = 'user_role_id';
}
