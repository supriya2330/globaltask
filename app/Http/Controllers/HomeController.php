<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\UserRole;
use App\User;
use PDF;
use Auth;

class HomeController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth',['except' => ['register','signup','login','signin']]);
        $this->middleware('guest',['except' => ['home','logout','profile','update_profile','export']]);
    }

	public function register()
    {
    	$user_roles = UserRole::get();
    	return view('register',compact('user_roles'));
    }

    public function signup(Request $request)
    {
    	$this->validate($request, [
            'email' => 'required|email|max:50',
            'name' => 'required|max:50',
            'user_role_id' => 'required',
            'password' => 'required|min:6',
        ]);

        $user = User::create([
        	'email' => $request->email,
        	'name' => $request->name,
        	'user_role_id' => $request->user_role_id,
        	'password' => Hash::make($request->password),
        ]);

        $data = array(
	        'email' => $request->email,
	        'password'=> $request->password
	    );

	    if(Auth::attempt($data)) 
	    {
	    	return redirect('/home');
	    }
	    else
	    {
	    	dd('hey');
	    	return redirect('/login');
	    }
    }

    public function login()
    {
    	return view('login');
    }

    public function signin(Request $request)
    {
    	$this->validate($request, [
            'email' => 'required|email|max:50',
            'password' => 'required|min:6',
        ]);

        $data = array(
	        'email' => $request->email,
	        'password'=> $request->password
	    );

	    if(Auth::attempt($data)) 
	    {
	    	return redirect('/home');
	    }
	    else
	    {
	    	return redirect('/login');
	    }
    }

    public function home()
    {
    	$users = User::paginate(5);
    	return view('home',compact('users'));
    }

    public function export(Request $request)
    {
    	$users = User::get();
    	if($request->type=='excel')
    	{
    		return view('export',compact('users','request'));
    	}
    	else
    	{
    		$pdf = PDF::loadView('export',compact('users','request'));
			return $pdf->stream('user.pdf');
    	}
    }

    public function profile()
    {
    	$user_roles = UserRole::get();
    	return view('profile',compact('user_roles'));
    }

    public function update_profile(Request $request)
    {
    	User::where('user_id',Auth::User()->user_id)->update([
    		'email' => $request->email,
        	'name' => $request->name,
        	'user_role_id' => $request->user_role_id,
    	]);
    	return redirect('/profile');
    }

    public function logout()
    {
    	Auth::logout();
    	return redirect('/login');
    }
}
